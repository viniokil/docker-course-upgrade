<!-- .slide: data-background="images/docker-background.png" -->

---
## Кто я такой?
---

## Кравец Валерий

- DevOps команды EvoPay
- Человеком с синдромом DevOps головного мозга
- Забавный пункт

---
## Node JS Docker image 
## меньше чем базовый

---

## А зачем было 
## паковать NodeJS в Docker?

---
<!-- .slide: style="text-align: left;" -->

### О проекте
![logo](https://cdn.chanty.com/landing/202006191500/images/home-page/logo-blue.svg)

Ссылка на проект: [Chanty](https://www.chanty.com/)

- продвинутый командный месседжер
- 15 микросвервисов
- backend на NodeJS

---

# Зачем оптизировать?

---

![i-tak-soydet.jpg](images/i-tak-soydet.jpg)

---

### Входные проблемы:
- размеры образов
- время старта новой версии
- time to market
- docker registry 200 Gb
- нет нормльного способа чистить docker registry
- проблемы с обновлением nodejs

---

# Пути оптимизации

---
<!-- .slide: style="text-align: left;" -->

### Простые:
- смена дистибутива базового image
- оптимизация слоев
- кеширование
- мультистейдженоговая сборка
- магические тулы

### Сложные:
- Distroless images
- магия JavaScript инструментария

---
### Смена дистибутива базового image

```Dockerfile
FROM node:12-buster
# 886MB
```
```Dockerfile
FROM node:12-stretch-slim
# 156MB
```
```Dockerfile
FROM node:12-alpine
# 89.3MB
```
---
<!-- .slide: style="text-align: left;" -->

### Работает для:
- NodeJS
- PHP
- Go
- Etc

### Не работает для:
- Python

---

## Оптимизация слоев

![layers-of-legos.gif](images/layers-of-legos.gif)

---

```Dockerfile
RUN apk add --no-cache --virtual .build-deps \
                            bash \
                            git \
                            build-base \
                            openssh-client
RUN npm i -g npm
RUN npm install
RUN npm build
# Cleanup build dependencies
RUN apk del -f .build-deps
```
---
```Dockerfile
RUN set -xe \
    && apk add --no-cache --virtual .build-deps \
                            bash \
                            git \
                            build-base \
                            openssh-client \
    && npm i -g npm \
    && npm install \
    && npm build \
    && apk del -f .build-deps
```
---
<!-- .slide: style="text-align: left;" -->
### Преимущества:
- Меньшее количество слоев
- Нет мусора в конечном image
- Размер конечного docker image
- Безопасность конечного имеджа

### Недостатки:
- Время сборки
- Невозможность переиспользования

---
## Мультистейдженоговая сборка
```Dockerfile
# Stage 1
FROM node:12-alpine as Builder
RUN set -xe \
    && apk add --no-cache \
                            git \
                            build-base \
                            openssh-client \
    && npm i -g npm \
    && npm install \
    && npm build

# Stage 2
FROM node:12-alpine as app
COPY --from=Builder /app/build /app
COPY --from=Builder /usr/share/misc/magic.mgc /usr/share/misc/magic.mgc
```
---
<!-- .slide: style="text-align: left;" -->
### Преимущества:
- Возможность вынести стейдж 1 отдельно
- Возможность кеширования и переиспользования
- Больше читабельности
- Нет забот с очисткой

### Недостатки:
- Необходимость понимать внешние зависимости
- Время конечной сборки

---
### Магические тулы
- [docker-slim](https://github.com/docker-slim/docker-slim
)
- Опытный колега

---
<!-- .slide: data-background="images/true-hacker.gif" -->
---
### Distroless images
- [от Google](https://github.com/GoogleContainerTools/distroless)
- свой
---

### Почему свой?
| Image                    | Size Node v10  | Node version |
| ---                      | ---    | ----          |
| gcr.io/distroless/nodejs | 81 MB | only 10.17    |
| node:10-alpine           | 77 MB | from 10 to 14 |
| own image                | 42MB  | any           |

---

### Как сделать свой Distroless image
```Dockerfile
FROM node:12-alpine as base
RUN apk add \
            binutils \
            ca-certificates \
            gawk \
    && mkdir /build \
    && strip --strip-all /usr/local/bin/node \
    && ldd /usr/local/bin/node \
            | awk '/=>/{print $3}' \
            | while read LIB_PATH; \
            do cp --parents $LIB_PATH /build/;\
            done \
    && cp --parents /etc/ssl/certs/ca-certificates.crt /build/

FROM scratch
COPY --from=base /usr/local/bin/node /usr/local/bin/node
COPY --from=base /build/ ""
RUN ["/usr/local/bin/node", "-v"]
```

---
<!-- .slide: style="text-align: left;" -->
### Преимущества:
- Экономия места
- Безопасность

### Недостатки:
- Не всем подойдет отсутствие shell и core-utils
- Необходимость понимать и прописывать внешние зависимости

---

![node_modules.jpg](images/node_modules.jpg)

---
<!-- .slide: style="text-align: left;" -->
### JavaScript инструментарий
- webpack
- webpack-merge

---
### `webpack.config.js`
```js
var path = require('path');
var webpack = require('webpack');
var fs = require('fs');

const merge = require('webpack-merge');
const tools = require('server-lib/tools/build');

var entries = {};
for (var i = 0; i < tools.length; i++) {
	entries[tools[i]] = './node_modules/server-lib/tools/'+tools[i]+'.js';
}
console.log(entries)

module.exports = merge(
	require('server-utils/webpack.config.js'),
	require('server-lib/webpack.config.js'),
	require('express-geoip/webpack.config.js'), {
		entry: Object.assign({}, entries, {
			server: './src/bin/server.js',
		}),
		target: 'node',
		mode: 'production',
		externals: {

		},
		resolve: {
			extensions: [".js"]
		},
		output: {
			filename: '[name].js',
			path: path.join(__dirname, 'build'),
		}
	});
```
---
### И что это дало?

| Object         | Size  |
| ---            | ---   |
| sources        | 0.15 MB |
| ./node_modules | 108  MB |
| server.js      | 15   MB |

---
### Что в итоге?
| Image                  | Size Node v12 |
| ---                    | ---           |
| node:10-alpine         | 77  MB        |
| own/node:12-distroless | 42  MB        |
| own/node:12-builder    | 386 MB        |
| app:web-api            | 60  MB        |

---
### Преимущества:
- Мы забыли о вопросах с Docker registry
- Практический мгновенное обновление на проде
- Использование build image для локальной разработки
- Огромная безопасность, нечего ломать
- Четка декларация зависимостей

---

### Недостатки:
- Не всем подойдет отсутствие shell и core-utils
- Сложность с осознанием своих зависимостей
- Требует времени и усилий

---
<img height="600" src="images/questions-3.jpg">

---

<img height="600" src="images/swarm.gif">
