### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---
<img src="images/gitlab-logo-gray-stacked-rgb.png"  height="50%" width="50%">

## CI/CD

---

# ДЗ

---

### Цель

Добавить `docker-compose.yml` в ваш текущий проект

---

### Что такое docker-compose 
### и чем отличаеться от docker-compose.yml

---

### Зачем нужен docker-compose?

---

### Что произойдет, если выполнить

`docker-compose up`

---

## CI/CD

---

## Что такое CI/CD



### CI - Continuous Integration

### CD - Continuous Delivery/Deployment

---

![wtf](images/wtf.jpg)

---

### CI - Continuous Integration

Постоянная интеграция нового кода с основной кодовой базой

---

### Continuous Integration

#### На этом этапе происходит

- сборка проекта
- тестирование
- мержи по результатам тестов

---

### Зачем это надо?

- паралельная работа над проектом
- частичная автоматизация ревью
- автоматизация тестов
- вынести процессы с локали на сервер

---

### Чем плох CI

- Его надо написать
- Сложно сказать - у меня билдиться =)

---

### CD - Continuous Delivery/Deployment

Постоянныая доставка или развертывание на сервера, по готовности кодовой базы

---

### CD - Continuous Delivery/Deployment
 
Упаковка проекта 
- docker image 
- dep/rpm package
- composer/npm package

---
### CD - Continuous Delivery/Deployment

Доставка (Continuous Delivery)
- пакета в репозиторий
- имеджа в docker registry

Развертывание (Continuous Deployment)
- Установка пакета
- Запуск контейнеров из имеджа 

---

### Чем хорошо

- Уменьшение рутины
- Увеличение частоты релизов
- Невиляция человеческого фактора
- Возможность дать деплоить даже менеджеру

---

### Чем плохо

- Все начинают забывать как это делаеться руками
- Невозможно автоматизировать то, что никогда не делали

---

# GitLab CI

---

### GitLab CI

Инструмент для реализации CI/CD:

- Тесно интегрирован с GitLab
- Всегда рядом с кодом
- Работает из коробки
- Бесплатен
- Прост
- Yaml

---

## Введение в Gitlab CI

---

### Шаг 1

Cоздаем репозиторий в https://gitlab.com
c названием `ci-example`

---

### Шаг 2

Клонируем репозиторий и открываем его у себя!

---

### Шаг 3

Создаем файл `app.sh`

- app.sh

```shell
touch app.sh
```
---

### Шаг 4

Наполняем файлы данными

```shell
echo '#!/bin/sh

    echo "Hello world!"
' > app.sh
```

---

### Шаг 5

Напишем свой первый CI

- Создаем файл `.gitlab-ci.yml` c таким содержимым

```yaml
test:
  script: sh app.sh | grep -q 'Hello world'
```

---

### Шаг 5

- Комитим и пушим в gitlab

```shell
git add .
git commit -am 'Inital commit'
git push
```

- Открываем проект на gitlab.com

---

![](images/gitlab-1.png)

---

### Cписок пайплайнов репозитория

![](images/gitlab-2.png)

---

### Cписок джоб в пайплайне

![](images/gitlab-3.png)

---

### Лог джобы

![](images/gitlab-4.png)

---
<p><span class="fragment">Теперь у нас есть СI?</span> <span class="fragment">Да</span></p>

<p><span class="fragment">Теперь у нас СI/CD?</span> <span class="fragment">Нет</span></p>

---

### Шаг 6

Обновим среду для тестов

`.gitlab-ci.yml`
```yaml
test:
  image: alpine
  script: sh app.sh | grep -q 'Hello world'
```
---

### Шаг 7

А файл точтно исполняемый?

Добавим еще один тест:

`.gitlab-ci.yml`
```yaml
test-functional:
  image: alpine
  script: sh app.sh | grep -q 'Hello world'

test-permissions:
  image: alpine
  script: test -x app.sh
```

---

### Шаг 7

- комитим
- пушим
- смотрим на результат

```shell
git commit -am "(add) test for permissions"
git push
```

---

<video controls>
  <source src="images/gitlab-step-7.mp4" type="video/mp4">
</video>

---

### Какой вывод мы можем сделать?

---

### Шаг 8

Чиним права


```shell
chmod +x app.sh
```

---

## Так, а где Docker?

---

### Шаг 9

Cоздадим Dockerfile

```Dockerfile
FROM alpine

COPY app.sh /app.sh

CMD [ "/app.sh" ]
```

---

### Шаг 10

Добаляем docker build

```yaml
build docker:
  image: docker:stable
  script:
  # Авторизация в гитлаб реджистри
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Билдим имедж с тегом вида `путь-к-проекту:latest`
  - docker build -t ${CI_REGISTRY_IMAGE}:latest ./
  # Пушим в имедж в реджистри
  - docker push ${CI_REGISTRY_IMAGE}:latest
```

Это похоже на рабочее решение, но это не так.

По скольку у нас есть docker client, но нет docker engine

---

---

### Шаг 11
Добавим рядышком сервив docker in docker (dind)

```yaml
build docker:
  image: docker:stable
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  - docker build -t ${CI_REGISTRY_IMAGE}:latest ./
  - docker push ${CI_REGISTRY_IMAGE}:latest
```

---

## А откуда взялись переменные?

- CI_REGISTRY
- CI_JOB_TOKEN
- CI_REGISTRY_IMAGE

[GitLab CI/CD environment variables](https://docs.gitlab.com/ce/ci/variables/#syntax-of-environment-variables-in-job-scripts)


---

### Шаг 12

- комитим
- пушим
- смотрим на результат

```shell
git add Dockerfile 
git commit -am "(add) dockerize app"
git push
```

---

<video controls>
  <source src="images/gitlab-step-12.mp4" type="video/mp4">
</video>

---

## Что не так?

---

### Шаг 13

Чиним последовательность

`.gitlab-ci.yml`
```yaml
stages:
  - test
  - build

test functional:
  image: alpine
  stage: test
  script: sh app.sh | grep -q 'Hello world'

test permissions:
  image: alpine
  stage: test
  script: test -x app.sh

build docker:
  image: docker:stable
  stage: build
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  - docker build -t ${CI_REGISTRY_IMAGE}:latest ./
  - docker push ${CI_REGISTRY_IMAGE}:latest

```
---

### Шаг 14

- комитим
- пушим
- смотрим на результат

```shell
git commit -am "(fix) CI steps"
git push
```

---

<video controls>
  <source src="images/gitlab-step-14.mp4" type="video/mp4">
</video>

---

![step-14](images/gitlab-step-14.png)

---

![rembo.jpeg](../images/rembo.jpeg)

---

# ДЗ

---

### Добавить docker build хотябы для одного имеджа в вашем проекте

---
