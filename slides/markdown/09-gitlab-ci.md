### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---
<img src="images/gitlab-logo-gray-stacked-rgb.png"  height="50%" width="50%">

## CI/CD

---

# ДЗ

---

## Повторить пример из занятия

Прошу скинуть ссылки на ваши репы `example-ci` мне в skype

---

### Добавить docker build при помощи GitLab CI 
### хотябы для одного имеджа в вашем проекте

---

# Вопросы

---

![questions.jpg](../images/questions.jpg)

---

## Что такое CI/CD

---

### Зачем нужен Continuous Integration

---

### Чем плох CI

---

### Какая разница между Continuous Delivery и Continuous Deployment

---

### Какие преимущества дает применение CD?

---

### Какие системы для CI/CD вы использовал?

---

### Какие особенности GitLab CI понравились?

---

### Какой формат используеться для GitLab CI?

---

`.gitlab-ci.yml`
```yaml
stages:
  - test
  - build

test functional:
  image: alpine
  stage: test
  script: sh app.sh | grep -q 'Hello world'

test permissions:
  image: alpine
  stage: test
  script: test -x app.sh

build docker:
  image: docker:stable
  stage: build
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  - docker build -t ${CI_REGISTRY_IMAGE}:latest ./
  - docker push ${CI_REGISTRY_IMAGE}:latest

```
---

![symfony.png](../images/symfony.png)

# CI для Symfony

---

### Шаг 1

Берем наш базовый проект

```

git clone git@gitlab.requestum.com:valerii.kravets/symfony-api-edition.git
cd symfony-api-edition
git checkout gitlab-ci
```

---

### Шаг 2

Смотрим README.md, где указанно:

1. как запустить проект локально
2. как установить зависимости
3. как проверить, что проект работает
4. как запустить тесты
5. как собрать проект

---

### Спойлер
![not_found.jpg](../images/not_found.jpg)

---

### Шаг 3

На основании этих данным строим структуру CI:

1. как запустить проект локально
2. как установить зависимости
3. как проверить, что проект работает
4. как запустить тесты
5. как собрать проект

---

### Шаг 4

На основании этих данным строим структуру CI:

1. Docker base build
2. Composer install
3. PHP Unit
4. Docker app build

`.gitlab-ci.yml`

```yaml
stages:
  - docker-build
  - test
  - docker-app

```

----


### Шаг 5

Исходя из docker-compose у нас есть три Dockerfile

- app
- nginx
- postgres-postgis

---

Добавим их билд в `.gitlab-ci.yml`

```yaml
...
build docker:
  image: docker:stable
  stage: docker-build
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build app
  - docker build -t ${CI_REGISTRY_IMAGE}/app:dev 
                -f ./docker/app/Dockerfile --target php-dev ./
  - docker push ${CI_REGISTRY_IMAGE}/app:dev
```

---

### Шах 6

Флаги и значения для `docker build` берем из `docker-compose.yml`

```shell
docker build -t ${CI_REGISTRY_IMAGE}/app:latest -f ./docker/app/Dockerfile --target php-dev ./
```

`docker-compose.yml`
```yml
services:
  # For local development
  &app-service app: &app-service-template
    image: registry.requestum.com/valerii.kravets/symfony-api-edition/app:latest
    build:
      context: ./
      dockerfile: ./docker/app/Dockerfile
      target: php-dev
```

---

### Шаг 7

Собираем все нужные имеджи:

```yaml
  ...
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build app
  - docker build -t ${CI_REGISTRY_IMAGE}/app:dev 
                -f ./docker/app/Dockerfile --target php-dev ./
  - docker push ${CI_REGISTRY_IMAGE}/app:dev
  # Build nginx
  - docker build -t ${CI_REGISTRY_IMAGE}/nginx:latest -f ./docker/nginx/Dockerfile ./
  - docker push ${CI_REGISTRY_IMAGE}/nginx:latest
  # Build postgres-postgis
  - docker build -t ${CI_REGISTRY_IMAGE}/postgres-postgis:latest 
                -f ./docker/postgres-postgis/Dockerfile  ./
  - docker push ${CI_REGISTRY_IMAGE}/postgres-postgis:latest
```

---

## Что не так?

---

### Шаг 8

Паралелим сборку:
```yaml
build app-dev:
  image: docker:stable
  stage: docker-build
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build app
  - docker build -t ${CI_REGISTRY_IMAGE}/app:dev 
                -f ./docker/app/Dockerfile --target php-dev ./
  - docker push ${CI_REGISTRY_IMAGE}/app:dev
```

---

```yaml
build nginx:
  image: docker:stable
  stage: docker-build
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build nginx
  - docker build -t ${CI_REGISTRY_IMAGE}/nginx:latest -f ./docker/nginx/Dockerfile ./
  - docker push ${CI_REGISTRY_IMAGE}/nginx:latest
```

---

```yaml
build postgres-postgis:
  image: docker:stable
  stage: docker-build
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build postgres-postgis
  - docker build -t ${CI_REGISTRY_IMAGE}/postgres-postgis:latest 
                -f ./docker/postgres-postgis/Dockerfile  ./
  - docker push ${CI_REGISTRY_IMAGE}/postgres-postgis:latest
```

---

# Тестирование

---

### Шаг 9

Установка пакетов и запуск тестов

```yaml
php unit:
  image: ${CI_REGISTRY_IMAGE}/app:dev
  stage: test
  script:
  - yum install php-pecl-xdebug.x86_64
  - cd ./src
  - composer install --no-interaction --ansi --no-suggest --prefer-dist
  - php ./vendor/bin/phpunit --colors=never --coverage-text
```
---

## Похоже на правду?

---

### Шаг 10

Добавляем сервисы

```yaml
php unit:
  image: ${CI_REGISTRY_IMAGE}/app:dev
  stage: test
  variables:
    DB_CONNECTION: 'pgsql'
    POSTGRES_DB: &pg-db-name 'app'
    POSTGRES_USER: &pg-username 'app'
    POSTGRES_PASSWORD: &pg-password 'app'
    DB_HOST: 'postgres'
    DB_PORT: 5432
    DB_DATABASE: *pg-db-name
    DB_USERNAME: *pg-username
    DB_PASSWORD: *pg-password
    REDIS_HOST: 'redis'
    REDIS_PORT: 6379
  services:
  - ${CI_REGISTRY_IMAGE}/postgres-postgis:latest
  - redis:4.0.10
  script:
  - yum install php-pecl-xdebug.x86_64
  - cd ./src
  - composer install --no-interaction --ansi --no-suggest --prefer-dist
  - php ./vendor/bin/phpunit --colors=never --coverage-text
```

---

### Шаг 11

Добавляем конфиг для docker

---

### Шаг 12

Билдим имедж с протестированным приложением

```yaml

build app-prod:
  image: docker:stable
  stage: docker-app
  services:
  - docker:dind
  script:
  - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
  # Build app
  - docker build -t ${CI_REGISTRY_IMAGE}/app:latest 
                -f ./docker/app/Dockerfile --target symfony-prod ./
  - docker push ${CI_REGISTRY_IMAGE}/app:latest
```

---

# ДЗ 

---

- Добавить полный CI цикл в ваше приложение на Symfony

Примеры:

https://gitlab.requestum.com/valerii.kravets/symfony-api-edition/tree/gitlab-ci


