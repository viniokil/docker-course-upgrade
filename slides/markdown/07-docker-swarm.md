### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---

![logo](images/docker-official.svg)

## Docker Swarm

---

# ДЗ

---

### Цель

- работающий web сервис
- в Docker
- защищенный ssl сертификатом
- доступный по домену

---

### Достижение

- работающий web сервис
- в Docker
- защищенный ssl сертификатом
- доступный по домену
- видел в глаза Portainer

---

### Что такое GCP?

---

### Где посмотреть ID проекта?

---

### Чем отличается gcloud от GCP?

---

### Для чего вы использовали gcloud?

---

### Как в будущем вы можете использовать GCP?

---

### Что делает 

`docker run -p 80:8080 sdinhbsm/go-ping`

---

### Что произойдет, если только-что создана docker-machine и выполнить

`docker-compose up`

---

### Что делает 

`docker-machine env vm-vinz`

---

### А че оно требует запуcка yaml только в том терминале где создавалась докер машина

---

### Как посмотреть адрес docker-machine?

---

### Где зарегать бесплатный домен?

---

![dns-a.png](../images/dns-a.png)

---

### Что тут не так? 

```shell
*.vinz.top  CNAME  300  vinz.top
```

### Что такое TTL в DNS?

---

## Docker сети

---

### Как посмотреть?

```shell
docker network ls
```

```config
NETWORK ID    NAME                         DRIVER  SCOPE
6231670a1c4e  artjokerua_backend-network   bridge  local
a05796e1bb2a  artjokerua_frontend-network  bridge  local
cbd6aedce1ae  bridge                       bridge  local
5f25c7b84875  host                         host    local
f7982537d53a  none                         null    local
```

---

### NETWORK ID 

Уникальное имя сети в Docker

```config
NETWORK ID
6231670a1c4e
```

---

### NAME

Человеческое имя


```bash
NAME
artjokerua_backend-network  # сеть из docker-compose
bridge # Дефолтная сеть с доступом в локалку и интернет
host # Дефолтная сеть с доступом к портам хоста и без изоляции
none # Дефолтная сеть с выключеным доступом в сеть
```
---

### DRIVER

Как должна работать сеть

```bash
DRIVER
bridge # Создать мостик в локальную сеть
host # Использовать сетевое устройство хоста
null # Вырубить сеть этому товарищу
```

---

### SCOPE

Где существует сеть

```conf
SCOPE
local # Cуществует только локально
```

---


```
NETWORK ID          NAME                          DRIVER              SCOPE
6231670a1c4e        artjokerua_backend-network    bridge              local
a05796e1bb2a        artjokerua_frontend-network   bridge              local
cbd6aedce1ae        bridge                        bridge              local
5f25c7b84875        host                          host                local
f7982537d53a        none                          null                local
```

---

### Тема сложная. Для детального разбора

Сети Docker изнутри: как Docker использует iptables и интерфейсы Linux

[https://habr.com/ru/post/333874/](https://habr.com/ru/post/333874/)

---

![Docker Swarm](../images/swarm.png)

## Docker Swarm

---

### Что такое Docker Swarm

Решение для управление множеством docker серверов и контейнеров

---

### Чем хорош

- Поставляется из коробки с Docker
- Разворачивается 1-й командой 
- Позволяет описывать конфигурацию в формате Docker Compose
- Есть встроенный Service Discovery
- Есть встроенный Load Balancer

---

### Чем плох

- Не использует внешние распределенные хранилища
- До 2-5 серверов
- До 100 контейнеров

---

### Swarm cluster

![swarm-cluster.png](../images/swarm-cluster.png)

---

# Строим свой кластер с...

---

### Создаем первого менеджера

```shell
docker-machine create swarm-manager \
            --driver google \
            --google-project $(gcloud config list --format 'value(core.project)') \
            --google-disk-size 25 \
            --google-disk-type pd-ssd \
            --google-preemptible \
            --google-tags http,https,swarm \
            --google-machine-image \
    https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20190212a 
```

---

### А он точно Swarm?

Не точно

```bash
eval $(docker-machine env swarm-manager)
docker swarm init
```

```
Swarm initialized: 
current node (2nmtlicoxx5purddosvre8s3g) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-5ymqtehqm940alc25
    10.128.0.26:2377

To add a manager to this swarm, run 
'docker swarm join-token manager' and follow the instructions.
```

---

<p><span class="fragment">Теперь у нас кластер?</span> <span class="fragment">Нет</span></p>

<p><span class="fragment">Теперь у нас Swarm?</span> <span class="fragment">Да</span></p>

---

Как убедится что Swarm включен

```shell
sudo docker node ls
```

---

На docker-machine

```
ID     HOSTNAME  STATUS  AVAILABILITY MANAGER STATUS  ENGINE VER
2nmt * vm-vinz   Ready   Active       Leader          18.09.2
```

В новом терминале

```shell
Error response from daemon: This node is not a swarm manager. 
Use "docker swarm init" or "docker swarm join" to connect 
this node to swarm and try again.
```

---

### Создадим воркера

```shell
docker-machine create swarm-worker-1 \
            --driver google \
            --google-project $(gcloud config list --format 'value(core.project)') \
            --google-disk-size 25 \
            --google-disk-type pd-ssd \
            --google-preemptible \
            --google-tags http,https,swarm \
            --google-machine-image \
    https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20190212a
```

---

### Добавим воркера в рой

```shell
eval $(docker-machine env swarm-worker-1)
docker swarm join <SWARM_MANAGER_INTERNAL_IP>:2377
```

---

### Список активных нод

Переключимся обратно на менеджера

```shell
eval $(docker-machine env swarm-manager)
```

```
docker node ls
```

---

### Создадим сервис

```
docker service create --replicas 1 -p 80:80/tcp --name nginx nginx
```

---

Смотрим список запущенных сервисов
```
docker service ls
```

```
ID   NAME  MODE       REPLICAS IMAGE        PORTS
trt  nginx replicated 1/1      nginx:latest *:80->80/tcp
```

---

### Увеличение количества реплик

Swarm обеспечит постоянную работу реплик

```
docker service scale nginx = 10
```

---

### Посмотрим на кластер из мира

```
gcloud compute instances list
```

```
NAME           PREEM INTERNAL_IP EXTERNAL_IP    
swarm-manager  true  10.128.0.28 104.154.244.128 
swarm-worker-1 true  10.128.0.29 35.194.39.216   
```

---

### А что, если nginx будет 1?

```
docker service scale nginx = 1
```

---

### Что делать с доменом? 

* Направить на manager
* Использовать DNS Round Robin
* Использовать Load Balancer

---

### Так IP меняется

У GCP Free tier ограничение в 1 IP

---

### Решение: 

Google Cloud Load Balancer

* Использует 1 IP для всех сервисов
* Проверяет живость хостов
* Обеспечивает работу отказоустойчивой системы

[http://console.cloud.google.com/networking/loadbalancing/list](http://console.cloud.google.com/networking/loadbalancing/list)

---

![create-load-balancer.png](../images/create-load-balancer.png)

---

![tcp-load-balancer.png](../images/tcp-load-balancer.png)

---

![lb-select.png](../images/lb-select.png)


---

![lb-name.png](../images/lb-name.png)

---


![lb-backend.png](../images/lb-backend.png)

---

![lb-region.png](../images/lb-region.png)

---

![lb-health.png](../images/lb-health.png)

---

![lb-ping.png](../images/lb-ping.png)

---

![lb-frontend.png](../images/lb-frontend.png)

---

![lb-frontend.png](../images/lb-frontend.png)

---

![lb-frontend.png](../images/lb-frontend.png)

---

![lb-frontend.png](../images/lb-frontend.png)

---

### Получаем IP лоад балансера

gcloud compute forwarding-rules list

---

![rembo.jpeg](../images/rembo.jpeg)

---

# ДЗ

---

# неть