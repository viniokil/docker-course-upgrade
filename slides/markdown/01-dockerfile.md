### Docker для продолжающих

<small>Created by [Valerii Kravets](viniokil@gmail.com) / [@viniokil](https://t.me/viniokil)</small>

---
![logo](images/docker-official.svg)

## Dockerfile


---

## О домашнем задании

---

## История

### Мы хотим собирать приложение

---

### Зачем?

---

### Чтобы оно работало

---

### Чтобы оно работало
<span class="fragment"> ... предсказуемо...</span>

<span class="fragment"> ... всегда ... </span>

<span class="fragment"> ... везде ... </span>

---

### Как это сделать?

Ваши варианты

---
<!-- .slide: style="text-align: left;" -->
## Что такое Dockerfile?

- Набор инструкций
- Руководство сборки
- Описание приложения

---

## Как он выглядит?

```Dockerfile
FROM node:11-alpine
LABEL Description="Application container"
ARG ANGULAR_VER=7.3.8
ENV PATH /scripts:/scripts/aliases:$PATH

RUN apk add --no-cache \
                git \
                openssh-client \
                bash \
        && yarn global add @angular/cli@$ANGULAR_VER

WORKDIR /app
COPY ["package.json","*-lock.json", "yarn.lock", "/app/"]
RUN if [ -f yarn.lock ]; then yarn install; else npm install; fi
COPY ./ /app
EXPOSE 4200
CMD yarn start
```

---

## Инструкции Dockerfile


---
<!-- .slide: style="text-align: left;" -->
### FROM
```Dockerfile
FROM node:11-alpine
```
Использовать за основу имедж из hub.docker.com

[Docker Hub Node](https://hub.docker.com/_/node/)

[11/alpine/Dockerfile)](https://github.com/nodejs/docker-node/blob/ed4cf449b40da125d4a5991fc12db20be51e6d4a/11/alpine/Dockerfile)

---
<!-- .slide: style="text-align: left;" -->
### LABEL

```Dockerfile
LABEL Description="Application container"
```
```Dockerfile
LABEL key="value"
```
Описание docker image'a в формате ключ="Значения"

---
<!-- .slide: style="text-align: left;" -->
### LABEL

```Dockerfile
LABEL Maintainer="valerii.kravets@fabware.com"
```

Указание автора

---
<!-- .slide: style="text-align: left;" -->
### ENV

```Dockerfile
ENV PS1='\[\033[1;32m\]🐳  \[\033[1;36m\][\u@\h] \[\033[1;34m\]\w\[\033[0;35m\] \[\033[1;36m\]# \[\033[0m\]'
ENV PATH /scripts:/scripts/aliases:$PATH
```

Указание переменных окружения

Для их просмотра в термимале:
- **env**
- **echo $PATH**

**PS1** - описание приглашения для ввода

**PATH** - где искать исполняемые файлы

---
<!-- .slide: style="text-align: left;" -->
### RUN

```Dockerfile
RUN apk add --no-cache \
                git \
                openssh-client \
                bash \
        && yarn global add @angular/cli
```

**RUN** команда, которую необходимо запустить во время сборки

**apk** - менеджер пакетов в Alpine linux. Аналог **apt** в Ubuntu

Описана установка пакетов с удалением кеша 

---
<!-- .slide: style="text-align: left;" -->
### RUN

```Dockerfile
RUN node -v \
    && npm-fpm -v \
    && yarn -v
```

Выполнение ряда инструкций в одном слое. 
Необходимо для экономии слоев

---

### COPY
<!-- .slide: style="text-align: left;" -->
```Dockerfile
COPY ./ /app
```

Скопировать все из текущей папки в папку /app в имедже

---

```Dockerfile
COPY ["package.json","*-lock.json", "yarn.lock", "/app/"]
```
Скопировать указанные файлы в папку /app в имедже

---
<!-- .slide: style="text-align: left;" -->
### WORKDIR

```Dockerfile
WORKDIR /app
```

С помощью WORKDIR можно установить рабочую директорию, в которой будут запускаться команды 

---
<!-- .slide: style="text-align: left;" -->

### CMD

```Dockerfile
CMD yarn start
```

Инструкция CMD указывает, какую команду необходимо запустить, когда контейнер запущен. В отличие от команды RUN указанная команда исполняется не во время построения образа, а во время запуска контейнера. 

---
<!-- .slide: style="text-align: left;" -->
### ENTRYPOINT

```Dockerfile
ENTRYPOINT /bin/bash
```
Часто команду CMD путают с ENTRYPOINT.

CMD - передается в ENTRYPOINT

---
<!-- .slide: style="text-align: left;" -->

```Dockerfile
ENTRYPOINT /bin/bash
CMD ls -alh
```

Можно комбинировать ENTRYPOINT и CMD.

В этом случае команда в ENTRYPOINT выполнится в любом случае, а команда в CMD выполнится, если не передано другой команды при запуске контейнера. Если требуется, вы все-таки можете изменить команду ENTRYPOINT с помощью флага --entrypoint.

---
<!-- .slide: style="text-align: left;" -->
### USER

Конкретихирует пользователя, под которым должен быть запущен образ. Мы можем указать имя пользователя или UID и группу или GID.

```Dockerfile
USER user
USER user:group
USER uid
USER uid:gid
USER user:gid
USER uid:group
```

Вы можете изменить эту команду, используя флаг -u при запуске контейнера. Если пользователь не указан, используется root по-умолчанию.

---
<!-- .slide: style="text-align: left;" -->
### VOLUME

```Dockerfile
VOLUME ["/opt/project"]
```

Инструкция VOLUME добавляет тома в образ. 

---

Том — папка в одном или более контейнерах или папка хоста, проброшенная через Union File System (UnionFS/UFS).

Тома могут быть расшарены или повторно использованы между контейнерами. Это позволяет добавлять и изменять данные без коммита в образ.

---

В примере выше создается точка монтирования /opt/project для любого контейнера, созданного из образа. 

Таким образом вы можете указывать и несколько томов.

---
<!-- .slide: style="text-align: left;" -->
### ADD

Инструкция ADD добавляет файлы или папки из нашего билд-окружения в образ, что полезно например при установке приложения.
```Dockerfile
ADD software.lic /opt/application/software.lic
```


---
<!-- .slide: style="text-align: left;" -->
### ADD

Источником может быть URL, имя файла или директория.

```Dockerfile
ADD http://wordpress.org/latest.zip /root/wordpress.zip
```

```Dockerfile
ADD latest.tar.gz /var/www/wordpress/
```

В последнем примере архив tar.gz будет распакован в /var/www/wordpress. Если путь назначения не указан — будет использован полный путь включая директории.

---
<!-- .slide: style="text-align: left;" -->
### COPY

```Dockerfile
COPY conf.d/ /etc/apache2/
```

Инструкция COPY отличается от ADD тем, что предназначена для копирования локальных файлов из билд-контекста и не поддерживает распаковки файлов:

---
<!-- .slide: style="text-align: left;" -->
### ONBUILD

```Dockerfile
ONBUILD ADD . /app/src
ONBUILD RUN cd /app/src && make
```

Инструкция ONBUILD добавляет триггеры в образы. Триггер исполняется, когда образ используется как базовый для другого образа, например, когда исходный код, нужный для образа еще не доступен, но требует для работы конкретного окружения.

---

# ДЗ

---
<!-- .slide: style="text-align: left;" -->
### Собрать в Dockerfile

* NodeJS приложение
* PHP приложение

---
<!-- .slide: style="text-align: left;" -->
* Коментировать каждую строчку в Dockerfile
* Сдать до вторника включительно
* Записать возникающие вопросы

---
<!-- .slide: style="text-align: left;" -->
## NodeJS приложение

* [nodeadmin](https://www.npmjs.com/package/nodeadmin)

[Руководство по написанию Dockerfile для веб-приложений на Node.js](https://medium.com/devschacht/praveen-durairaj-an-exhaustive-guide-to-writing-dockerfiles-for-node-js-web-apps-7b033bcc0b4f)

---

## Спасибо!

## Вы крутые!